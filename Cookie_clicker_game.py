# Cookie Clicker Game
# Made by Emil Wiberg
import turtle

wn = turtle.Screen()
wn.title("Cookie Clicker by @EmilWiberg")
wn.bgcolor("orange")

wn.register_shape("cookie 2.gif")
wn.register_shape("cookie hidden.gif")

cookie = turtle.Turtle()
cookie.shape("cookie 2.gif")
cookie.goto(0,0)
cookie.speed(0)

clicks = 0

pen = turtle.Turtle()
pen.hideturtle()
pen.color("red")
pen.penup()
pen.goto(0,200)
pen.write(f"Clicks: {clicks}", align="center", font=("Times New Roman", 28, "normal"))

def clicked(x,y):
    global clicks
    clicks += 1
    pen.clear()
    pen.write(f"Clicks: {clicks}", align="center", font=("Times New Roman", 28, "normal"))

cookie.onclick(clicked)

cookie = turtle.Turtle()
cookie.shape("cookie hidden.gif")
cookie.goto(200,0)
cookie.speed(0)

def clicked(x,y):
    global clicks
    clicks += 1
    pen.clear()
    pen.write(f"Clicks: {clicks}", align="center", font=("Times New Roman", 28, "normal"))


cookie.onclick(clicked)

cookie = turtle.Turtle()
cookie.shape("cookie hidden.gif")
cookie.goto(-200,0)
cookie.speed(0)

def clicked(x,y):
    global clicks
    clicks += 1
    pen.clear()
    pen.write(f"Clicks: {clicks}", align="center", font=("Times New Roman", 28, "normal"))


cookie.onclick(clicked)

cookie = turtle.Turtle()
cookie.shape("cookie hidden.gif")
cookie.goto(-200,-200)
cookie.speed(0)

def clicked(x,y):
    global clicks
    clicks += 1
    pen.clear()
    pen.write(f"Clicks: {clicks}", align="center", font=("Times New Roman", 28, "normal"))


cookie.onclick(clicked)

cookie = turtle.Turtle()
cookie.shape("cookie hidden.gif")
cookie.goto(200,-200)
cookie.speed(0)

def clicked(x,y):
    global clicks
    clicks += 1
    pen.clear()
    pen.write(f"Clicks: {clicks}", align="center", font=("Times New Roman", 28, "normal"))


cookie.onclick(clicked)

cookie = turtle.Turtle()
cookie.shape("cookie hidden.gif")
cookie.goto(-200,200)
cookie.speed(0)

def clicked(x,y):
    global clicks
    clicks += 1
    pen.clear()
    pen.write(f"Clicks: {clicks}", align="center", font=("Times New Roman", 28, "normal"))


cookie.onclick(clicked)

cookie = turtle.Turtle()
cookie.shape("cookie hidden.gif")
cookie.goto(200,200)
cookie.speed(0)

def clicked(x,y):
    global clicks
    clicks += 1
    pen.clear()
    pen.write(f"Clicks: {clicks}", align="center", font=("Times New Roman", 28, "normal"))


cookie.onclick(clicked)

wn.mainloop()
