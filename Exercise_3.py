Exercise 1: What is the function of the secondary memory in a computer? a) Execute all of the computation and logic of the program b) Retrieve web pages over the Internet c) Store information for the long term, even beyond a power cycle d) Take input from the user 
Answer is C

Exercise 2: What is a program? 
“A program is a sequence of python statements commanded to do something” from the book.

Exercise 3: What is the difference between a compiler and an interpreter? 
A compiler needs a whole file to read commands in a program, but the interpreter reads all the commands that has been written on the fly.

Exercise 4: Which of the following contains “machine code”? a) The Python interpreter b) The keyboard c) Python source file d) A word processing document 
Answer is B

Exercise 5: What is wrong with the following code: 1.14. EXERCISES 17 >>> primt 'Hello world!' File "", line 1 primt 'Hello world!' ^ SyntaxError: invalid syntax >>> 
There is a spelling error. Instead of “primt”, what it should say is “print”

Exercise 6: Where in the computer is a variable such as “x” stored after the following Python line finishes? x = 123 a) Central processing unit b) Main Memory c) Secondary Memory d) Input Devices e) Output Devices 
Answer is C

Exercise 7: What will the following program print out: x = 43 x = x + 1 print(x) a) 43 b) 44 c) x + 1 d) Error because x = x + 1 is not possible mathematically 
Answer is B

Exercise 8: Explain each of the following using an example of a human capability: (1) Central processing unit, (2) Main Memory, (3) Secondary Memory, (4) Input Device, and (5) Output Device. For example, “What is the human equivalent to a Central Processing Unit”? 
CPU is like the mind, it tells you what to do. And when you have decided, the mind will controls your body to do whatever you decided. This Is just like the cpu, that controls the other components in the pc. 
Main memory is your conscious. You are focused on something right now, and when you are done with that - you memory of that will fade away. This like the main memory, which vanishes after the pc is turned off. 
Secondary memory is your subconscious. It will layer information, and save it so next time something happens, you will have it ready to go.
Input device is your senses(what you take in). This you will registrate and reflect upon, just like the input on computers.
Output devices is your voice. This will come out of you, which means you are producing your own voice. Output devices makes the pc produce something, and the outputs deliver them.

Exercise 9: How do you fix a “Syntax Error”?
Syntax error means that you have written something wrong. The way to fix this is simply to go and fix the spelling mistake you have made.


